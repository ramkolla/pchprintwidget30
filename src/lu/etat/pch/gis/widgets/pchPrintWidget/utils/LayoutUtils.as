package lu.etat.pch.gis.widgets.pchPrintWidget.utils
{
	import com.esri.ags.geometry.MapPoint;

	public class LayoutUtils
	{
		public static function calculateAnchorPoint(anchorPoint:String, borderWidth:Array,printOutput:Object, xOffset:Number=0, yOffset:Number=0):MapPoint {
			var pt:MapPoint=new MapPoint();
			var borderLeft:Number=Number(borderWidth[0]);
			var borderRight:Number=Number(borderWidth[1]);
			var borderTop:Number=Number(borderWidth[2]);
			var borderBottom:Number=Number(borderWidth[3]);
			
			if(anchorPoint.substr(0, 3)=='top') {
				pt.y=Number(printOutput.height)-borderTop;
			} else if(anchorPoint.substr(0, 3)=='mid') {
				pt.y=borderBottom+((Number(printOutput.height)-borderBottom-borderTop)/2);
			} else if(anchorPoint.substr(0, 6)=='bottom') {
				pt.y=borderBottom;
			}
			if(anchorPoint.indexOf("left", 3)>0) {
				pt.x=borderLeft;
			} else if(anchorPoint.indexOf("mid", 3)>0) {
				pt.x=borderLeft+((Number(printOutput.width)-borderRight-borderLeft)/2);
			} else if(anchorPoint.indexOf("right", 3)>0) {
				pt.x=Number(printOutput.width)-borderRight;
			}
			pt.x=pt.x+xOffset;
			pt.y=pt.y+yOffset;
			return pt;
		}
	}
}