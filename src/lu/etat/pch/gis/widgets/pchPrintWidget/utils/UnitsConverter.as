package lu.etat.pch.gis.widgets.pchPrintWidget.utils
{
	import com.esri.ags.Units;
	
	public class UnitsConverter
	{
		public function UnitsConverter()
		{
		}
		
		public static function convertStringToUnit(s:String):String {
			if(s) {
				s=s.toLowerCase();
				if(s=='in') return Units.INCHES;
				if(s=='inch') return Units.INCHES;
				if(s=='inches') return Units.INCHES;
				if(s=='esriInches'.toLowerCase()) return Units.INCHES;
				if(s=='cm') return Units.CENTIMETERS;
				if(s=='esriCentimeters'.toLowerCase()) return Units.CENTIMETERS;
				if(s=='m') return Units.METERS;
				if(s=='esriMeters'.toLowerCase()) return Units.METERS;
				if(s=='ft') return Units.FEET;
				if(s=='feet') return Units.FEET;
				if(s=='esriFeet'.toLowerCase()) return Units.FEET;
				if(s=='dd') return Units.DECIMAL_DEGREES;
				if(s=='esriDecimalDegrees'.toLowerCase()) return Units.DECIMAL_DEGREES;
			}
			trace("UnitsConverter.convertStringToUnit.UNKNOWN units!!");
			return Units.UNKNOWN_UNITS;
		}
		
		public static function convertUnits(from:String,to:String,value:Number):Number {
			var fromUnit:String = convertStringToUnit(from);
			var toUnit:String = convertStringToUnit(to);
			if(fromUnit == Units.UNKNOWN_UNITS) {
				trace("UnitsConverter.convertUnits.UNKNOWN_FROM_UNITS: "+fromUnit+" -> "+toUnit+" !!");
				return value;
			}
			if(fromUnit == toUnit) return value;
			if(fromUnit==Units.CENTIMETERS && toUnit==Units.INCHES) return value/2.54;
			if(fromUnit==Units.CENTIMETERS && toUnit==Units.METERS) return value/100;
			if(fromUnit==Units.CENTIMETERS && toUnit==Units.FEET) return value/30.48;
			if(fromUnit==Units.CENTIMETERS && toUnit==Units.DECIMAL_DEGREES) return value/(1/11119487.22);
			
			if(fromUnit==Units.INCHES && toUnit==Units.CENTIMETERS) return value*2.54;
			if(fromUnit==Units.INCHES && toUnit==Units.METERS) return value*0.0254;
			if(fromUnit==Units.INCHES && toUnit==Units.FEET) return value/12;
			if(fromUnit==Units.INCHES && toUnit==Units.DECIMAL_DEGREES) return value/(1/4377742.119388889);
			trace("UnitsConverter.convertUnits.UNKNOWN case: "+fromUnit+" -> "+toUnit+" !!");
			return -1;
		}
	}
}